# !/usr/bin/env/python3
# coding: utf-8

from scipy.spatial import Delaunay
import numpy as np
import itertools


class MeshGen:
    @staticmethod
    def generate_scipy_delanay(points):
        """
        generates triangulated or tetrahedral mesh according to "points" dimension
        """
        tri = Delaunay(points)
        return tri

    @staticmethod
    def generate_rectangle_points_2d(p0, l, dims):
        """
        p0 - is list with reference points coordinates [x0, y0]
        l - is list with dims in each direction x, y
        dims -- is mesh dimenstion in each direction x,y
        """
        [x0, y0] = p0
        [lx, ly] = l
        [nx, ny] = dims
        x = np.linspace(x0, x0 + lx, nx)
        y = np.linspace(y0, y0 + ly, ny)
        xy = [list(d) for d in itertools.product(x, y)] 
        points = np.array(xy)
        # print(points)
        return points

    @staticmethod
    def generate_cube_points_3d(p0, l, dims):
        """
        p0 - is list with reference points coordinates [x0, y0, z0];
        l - is list with dims in each direction x, y,z
        dims -- is mesh dimenstion in each direction x,y,z
        """
        [x0, y0, z0] = p0
        [lx, ly, lz] = l
        [nx, ny, nz] = dims
        x = np.linspace(x0, x0 + lx, nx)
        y = np.linspace(y0, y0 + ly, ny)
        z = np.linspace(z0, z0 + lz, nz)
        xyz = [list(d) for d in itertools.product(x, y, z)] 
        points = np.array(xyz)
        print(points)
        return points
