# !/usr/bin/env/python3
# coding: utf-8

from enum import Enum
from save_mesh_vtk import VtkSaver


class MeshSaveOpt(Enum):
    VTK = 0 # legacy VTK (.vtk)
    MSH = 1 # medit mesh file (.msh)


def save_mesh(cells, nodes, name, mesh_caption, mshsvopt=MeshSaveOpt.VTK):
    """
    """
    if (mshsvopt == MeshSaveOpt.VTK):
        name = name + '.vtk'
        vtk_saver = VtkSaver()
        vtk_saver.save_tri(cells, nodes, name, mesh_caption)

    print('mesh saved to {}'.format(name))
