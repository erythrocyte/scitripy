# !/usr/bin/env/python3
# coding: utf-8


class VtkSaver:
    def save_tri(self, cells, nodes, name, mesh_caption):
        """
        cells -- scipy delanay mesh generator ouput type object
        nodes -- np array (both 2d or 3d)
        name -- file name
        mesh_caption -- caption of mesh (do not mess with field captions)
        """
        dim = len(nodes[0])
        f = open(name, 'w')
        print("GRID SAVING IN VTK FORMAT STARTED")
        cells_count = len(cells.simplices)
        points_count = len(nodes)

        self.__print_ascii_header(f, mesh_caption)
        self.__print_ascii_points_count(f, points_count)
        if (dim == 2):
            self.__print_ascii_points_2d(f, nodes)
        elif (dim == 3):
            self.__print_ascii_points_3d(f, nodes)
        else:
            self.__print_ascii_points_2d(f, nodes)

        one_cell_nodes_count = len(cells.simplices[0])
        self.__print_ascii_cells_simple(f, cells, one_cell_nodes_count)

        self.__print_ascii_cells_typecount(f, cells_count)

        # actually, this is not accurate classification. dim 2d and 3d could be anything
        # not only just triangle or tetrahedron
        cell_type = 5 if (dim == 2) else 10  # triangles -5, tetrahedron - 10
        self.__print_ascii_cells_type(f, cells_count, cell_type)

        self.__print_ascii_cellData_header(f, cells_count)

        self.__print_ascii_cell_data(f, "index", range(cells_count))

        print("GRID SAVING IN VTK FORMAT DONE IN {0} SEC".format(0))

        f.close()

    def __print_ascii_header(self, f, dataname):
        """
        """
        print("# vtk DataFile Version 3.0", file=f)
        print(dataname, file=f)
        print("ASCII", file=f)
        print("DATASET UNSTRUCTURED_GRID", file=f) 

    def __print_ascii_points_count(self, f, pts_count):
        """
        """
        print('{0} {1} {2}'.format("POINTS", pts_count, "double"), file=f)

    def __print_ascii_cells_count(self, f, cells_count, val_count):
        """
        """
        print('{0} {1} {2}'.format("CELLS", cells_count, val_count), file=f)

    def __print_ascii_cells_typecount(self, f, elem_count):
        """
        """
        print('{0} {1}'.format("CELL_TYPES", elem_count), file=f)

    def __print_ascii_cellData_header(self, f, cells_count):
        """
        """
        print('{0} {1}'.format("CELL_DATA", cells_count), file=f)

    def __print_ascii_scalars(self, f, cp):
        """
        """
        print('{0} {1} {2} {3}'.format("SCALARS", cp, 'float', 1), file=f)
        print("LOOKUP_TABLE default", file=f)

    def __print_ascii_vectors(self, f, cp):
        """
        """
        print('{0} {1} {2}'.format("VECTORS", cp, "float"), file=f)

    def __print_ascii_points_2d(self, f, points):
        """
        """
        for p in points:
            print('{0} {1} {2}'.format(p[0], p[1], 0), file=f)

    def __print_ascii_points_3d(self, f, points):
        """
        """
        for p in points:
            print('{0[0]} {0[1]} {0[2]}'.format(p), file=f)

    def __print_ascii_cells_type(self, f, elem_count, cell_type):
        """
        """
        for k in range(elem_count):
            print(cell_type, file=f)

    def __print_ascii_cell_data(self, f, field_cap, values):
        """
        values -- array of values in each cell;
        """
        self.__print_ascii_scalars(f, field_cap)

        for v in values:
            print(v, file=f)

    def __print_ascii_cells_2d_polyhedron(self, f, tri, tri_index=None):
        """
        """
        simplices = tri.simplices
        cells_count = len(simplices) if (tri_index is None) else len(tri_index)
        cell_indexes = range(cells_count) if (tri_index is None) else tri_index

        one_cell_faces_count = 3
        one_face_nodes_count = 2
        one_line_count = 1 + one_cell_faces_count * (one_face_nodes_count + 1)

        print('{0} {1} {2}'.format("CELLS", cells_count, (one_line_count + 1) * cells_count), file=f)

        for ci in cell_indexes:
            s = ''
            s = s + str(one_line_count) + ' '
            s = s + str(one_cell_faces_count) + ' '
            simplice = simplices[ci]

            # first face
            s = s + str(one_face_nodes_count) + ' ' + str(simplice[0]) + ' ' + str(simplice[1]) + ' '
            # second face
            s = s + str(one_face_nodes_count) + ' ' + str(simplice[1]) + ' ' + str(simplice[2]) + ' '
            # third face
            s = s + str(one_face_nodes_count) + ' ' + str(simplice[2]) + ' ' + str(simplice[0]) + ' '

            print(s, file=f)

    def __print_ascii_cells_simple(self, f, tri, one_cell_nodes_count, tri_index=None):
        """
        """
        simplices = tri.simplices
        cells_count = len(simplices) if (tri_index is None) else len(tri_index)
        cell_indexes = range(cells_count) if (tri_index is None) else tri_index

        print('{0} {1} {2}'.format("CELLS", cells_count, (one_cell_nodes_count + 1) * cells_count), file=f)

        for ci in cell_indexes:
            s = ''
            s = s + str(one_cell_nodes_count) + ' '
            simplice = simplices[ci]
            d = " ".join(map(str, simplice))
            s = s + d

            print(s, file=f)
