# !/usr/bin/env/python3
# coding: utf-8

from mesh_gen import MeshGen
import save_mesh


def main():
    # create 2d mesh in rectangle area
    # ref = [0.0, 0.0]
    # size = [1.0, 1.0]
    # dims = [2, 2]
    # nodes = MeshGen.generate_rectangle_points_2d(ref, size, dims)
    # tri = MeshGen.generate_scipy_delanay(nodes)
    # save_mesh.save_mesh(tri, nodes, 'tri', 'triangle', save_mesh.MeshSaveOpt.VTK)

    # creates 2d mesh in cube
    ref3d = [0.0, 0.0, 0.0]
    size3d = [1.0, 1.0, 1.0]
    dims3d = [3, 3, 3]
    nodes3d = MeshGen.generate_cube_points_3d(ref3d, size3d, dims3d)
    tetra = MeshGen.generate_scipy_delanay(nodes3d)
    save_mesh.save_mesh(tetra, nodes3d, 'cube_tetrahedron', 'tetrahedron', save_mesh.MeshSaveOpt.VTK)


if __name__ == '__main__':
    main()
